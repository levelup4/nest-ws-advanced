import { Injectable } from '@nestjs/common';

@Injectable()
export class UsersService {
  private readonly USERS = [
    {
      userId: 1,
      username: 'john',
      password: 'changeme',
    },
    {
      userId: 2,
      username: 'maria',
      password: 'guess',
    },
  ];

  async findOne(username: string) {
    return this.USERS.find((user) => user.username === username);
  }
}
