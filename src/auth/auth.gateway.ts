import { WebSocketGateway } from '@nestjs/websockets';

@WebSocketGateway(3000, { namespace: 'users', transports: ['websocket'] })
export class AuthGateway {}
